#include "Ustawienia.h"


void Ustawienia::Init()
{
	SetColor(Black);
	system("cls");
}

void Ustawienia::SetColor(Colors kolor)
{
	SetConsoleTextAttribute(_hCon, kolor);
}

void Ustawienia::Write(char ch, int x, int y)
{
	GotoXY(x, y);
	std::cout << ch;
}

void Ustawienia::Write(int i, int x, int y)
{
	GotoXY(x, y);
	std::cout << i;
}

void Ustawienia::Write(std::string tekst, int x, int y)
{
	GotoXY(x, y);
	//Zmiana kodowania tekstu na polski - pozwala na polskie znaki
	setlocale(LOCALE_ALL, "Polish");
	std::cout << tekst;
	//Zmiana kodowania tekstu na domy�lny - bez polskich znak�w
	setlocale(LOCALE_ALL, "C");
}

//Funkcja z neta
void Ustawienia::GotoXY(int x, int y)
{
	COORD c;
	c.X = x;
	c.Y = y;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
}
