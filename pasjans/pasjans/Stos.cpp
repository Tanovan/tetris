#include "Stos.h"



Stos::Stos()
{
}


Stos::~Stos()
{
}

void Stos::Dobierz(Talia *loc, int ile)
{
	for (int i = 0; i < ile; i++)
	{
		stos.push_back(loc->talia[0]);
		loc->talia.erase(loc->talia.begin());
	}
	stos[stos.size() - 1].SetKarZak(false);
};

void Stos::PokazStos(int x, int y)
{
	for (int i = 0; i < stos.size(); i++)
	{
		switch (stos[i].GetKarNum())
		{
		case 0:
			Write("A", x, y+i);
			break;

		case 10:
			Write("J", x, y + i);
			break;

		case 11:
			Write("Q", x, y + i);
			break;

		case 12:
			Write("K", x, y + i);
			break;

		default:
			Write(stos[i].GetKarNum() + 1, x, y + i);
			break;
		}
		Write(char(stos[i].GetKarZna() + 3), x + 3, y + i);
	}
};

void Stos::PokazStosUkryty(int x, int y)
{
	for (int i = 0; i < stos.size(); i++)
	{
		if (stos[i].GetKarZak() == false)
		{
			switch (stos[i].GetKarZna())
			{
			case 0:
				SetColor(Red);
				break;
			case 1:
				SetColor(Red);
				break;
			case 2:
				SetColor(Black);
				break;
			case 3:
				SetColor(Black);
				break;
			}

			Write(char(stos[i].GetKarZna() + 3), x , y + i);

			switch (stos[i].GetKarNum())
			{
			case 0:
				Write("A", x + 2, y + i);
				break;

			case 10:
				Write("J", x + 2, y + i);
				break;

			case 11:
				Write("Q", x + 2, y + i);
				break;

			case 12:
				Write("K", x + 2, y + i);
				break;

			default:
				Write(stos[i].GetKarNum() + 1, x + 2, y + i);
				break;
			}

			
			SetColor(Black);
		}
		else
		{
			SetColor(Black);
			Write("XXXX", x, y + i);

		}
	}
};

bool Stos::PrzeniesDo(Stos *dokad, int ile)
{
	vector <Karta> bufor;

	if (SprawdzCzyMoznaPrzeniesc(dokad, ile))
	{
		for (int i = 0; i < ile; i++)
		{
			bufor.push_back(stos[stos.size() - 1]);
			stos.erase(stos.end() - 1);
		}

		for (int i = 0; i < ile; i++)
		{
			dokad->stos.push_back(bufor[bufor.size() - 1]);
			bufor.erase(bufor.end() - 1);
		}
		if (stos.size() > 0)
		{
			stos[stos.size() - 1].SetKarZak(false);
		}
		return true;
	}
	else
	{
		return false;
	}
	
};

bool Stos::SprawdzKolejnosc()
{
	//czy na stosie jest wi�cej jak 13 kart, aby nie przejecha� poza zakres i w og�le sprawdzi� czy ma sens sprawdza� dalej(od A do K jest 13 kart)
	if (stos.size() >= 13) 
	{
		//sprytne sprawdzanie kolejno�ci. Podajemy pocz�tek zakresu, koniec i spos�b sprawdzania.
		if (is_sorted(stos.end() - 13, stos.end(), checkNumb()))
		{
			//sprawdza kolory
			if (Check())
			{
				return true;
			}
		}
	}
	return false;
}

bool Stos::Check()
{
	//potrzebna zmienna.
	bool flag = true;
	//sprawdzamy od ko�ca 13 kart
	for (int i = stos.size() - 1; i > stos.size() - 13; i--)
	{
		//czy znak aktualnej jest taki sam jak znak nast�pnej licz�c od do�u
		
		if (stos[i].GetKarZna() == stos[i - 1].GetKarZna() && flag == true)
		{
			//musimy sprawdzi� wszystki 13 dlatego nie mozna dac return true, bo by wysz�o z p�tli. Trzeba to zapami�ta�
			flag = true;
		}
		else
		{
			//Jak jedna warto�� z tego zakresu jest inna, to zwracamy false i ko�czymy sprawdzanie.
			flag = false;
			return false;
		}
	}
	//jak wszystkie s� zgodne, to zwracamy true.
	if (flag == true)
	{
		return true;
	}
}

bool Stos::SprawdzCzyMoznaPrzeniesc(Stos *dokad, int ile)
{
	if (dokad->stos.size() > 0)
	{
		if (is_sorted(stos.end() - ile, stos.end(), checkNumb()))
		{
			if (stos[stos.size() - ile].numer == dokad->stos[dokad->stos.size() - 1].numer - 1)
			{
				return true;
			}
			else
			{
				Write("Nie mo�na przenie��! Warto�� przenoszonego elementu nie zgodna z warto�ci� wierzchniej karty.", 1, 23);
				return false;
			}
		}
		else
		{
			Write("Nie mo�na przenie��! Karty kt�re pr�bujesz przenie�� nie s� posegregowane.", 1, 23);
			return false;
		}
	}
	else
	{
		return true;
	}
};

