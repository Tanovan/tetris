#pragma once

#include "Ustawienia.h"

class Karta : public Ustawienia
{
	bool zakryta;
	int znak;
public:
	int numer;
	Karta();
	~Karta();
	void KartaSet(int num, int zna, bool zak);
	int GetKarNum();
	bool GetKarZak();
	int GetKarZna();
	void SetKarZak(bool zak);
};
