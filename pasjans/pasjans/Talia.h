#pragma once
#include "Karta.h"
class Talia : public Karta
{
public:
	vector <Karta> talia;
	Talia();
	~Talia();
	void BuildTalia(int ileKolorow);
	void Wypisz();
	void Tasuj();
};

