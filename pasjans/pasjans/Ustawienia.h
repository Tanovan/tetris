#pragma once
#include <iostream>
#include <conio.h>
#include <string>
#include <vector>
#include <algorithm>
#include <Windows.h>


using namespace std;

class Ustawienia
{

	//Uchwyt okienka - pozwala zmienia� jego parametry
	HANDLE _hCon = GetStdHandle(STD_OUTPUT_HANDLE);

	//Przejd� na dan� pozycj� w konsoli
	void GotoXY(int x, int y);


public:

	//Przypisanie warto�ci� Black i Red parametr�w kt�re zmieniaj� kolor tekstu na podany
	enum Colors
	{
		Black = (BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED | BACKGROUND_INTENSITY),
		Red = (FOREGROUND_RED | FOREGROUND_INTENSITY | BACKGROUND_INTENSITY | BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED | BACKGROUND_INTENSITY),
	};

	//Zmienia kolor t�o na bia�e i tekst na czarny
	void Init();

	//Zmienia kolor tekstu
	void SetColor(Colors kolor);

	//Pisz zaczynaj�c od wskazanych koord�w
	void Write(char ch, int x, int y);

	//Pisz zaczynaj�c od wskazanych koord�w
	void Write(int i, int x, int y);

	//Pisz zaczynaj�c od wskazanych koord�w
	void Write(std::string tekst, int x, int y);

};

