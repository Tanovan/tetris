﻿#include "Gra.h"


Gra::Gra()
{
}


Gra::~Gra()
{
}

void Gra::CreateTab()
{
	for (int i = 0; i < 10; i++)
	{
		Stos d;
		tab.push_back(d);
	}
};

void Gra::FillTab(int od, int to, int ile, Talia *tal)
{
	for (int i = od - 1; i < to; i++)
	{
		Stos d;
		d.Dobierz(tal, ile);
		tab.push_back(d);
	}
};

void Gra::AddToTab(int od, int to, int ile, Talia *tal)
{
	for (int i = od - 1; i < to; i++)
	{
		tab[i].Dobierz(tal, ile);
	}
}

void Gra::Intro()
{
	Write("Pasjans Pająk", 35, 2);

	Write("Celem gry jest ułożenie stosu złożonego z upożądkowanych kart", 1, 5);
	Write("tego samego koloru (K,Q,J,10,9,8,7,6,5,4,3,2,A).", 1, 6);

	Write("Karty można układać na przemian kolorami.", 1, 8);

	Write("Karty można układać tylko mniejszą na większą (np. 7 na 8).", 1, 10);

	Write("Aby dołożyć do każdego stosu po jednej karcie należy w polach", 1, 12);
	Write("\"skąd, dokąd i ile\" wpisać wartość 0.", 1, 13);

	Write("Naciśnij dowolny klawisz, aby rozpocząć grę...", 1, 15);
	_getch();

};

void Gra::GameLoop(Talia *talia)
{
	do
	{
		//Funkcja ma pokazywać stosy
		PokazStosy(2, 1);

		int from, to, count;
		do
		{
			do
			{
				do
				{
					PokazStosy(2, 1);
					Write("Punkty: ", 35, 22); cout << punkty;
					Write("Pozostałych dobrań: ", 55, 22); cout << talia->talia.size() / 10;
					Write("Skąd: ", 1, 20);
					cin >> from;
				} while (from < 0 || from > 10);

				PokazStosy(2, 1);
				Write("Punkty: ", 35, 22); cout << punkty;
				Write("Pozostałych dobrań: ", 55, 22); cout << talia->talia.size() / 10;
				Write("Skąd: ", 1, 20); cout << from;
				Write("Dokąd: ", 1, 21);
				cin >> to;
			} while (to < 0 || to > 10);

			PokazStosy(2, 1);
			Write("Punkty: ", 35, 22); cout << punkty;
			Write("Pozostałych dobrań: ", 55, 22); cout << talia->talia.size() / 10;
			Write("Skąd: ", 1, 20); cout << from;
			Write("Dokąd: ", 1, 21); cout << to;
			Write("Ile: ", 1, 22);
			cin >> count;
		} while (count < 0);
		if (from > 0 && to > 0 && count > 0 && from < 10 && to < 10 && count <= tab[from - 1].stos.size())
		{
			// jak się udalo przenieć, to odejmij punkt
			if (tab[from - 1].PrzeniesDo(&tab[to - 1], count))
			{
				Write("Pomyślnie wykonano ruch", 1, 23);
				punkty--;
			}
		}
		else
		{
			if (from == 0 && to == 0 && count == 0)
			{
				if (talia->talia.size() > 0)
				{
					//jak poda się 3 zera, to dobiera karty
					AddToTab(1, 4, 1, talia);
					AddToTab(5, 10, 1, talia);
					Write("Dobrano karty.", 1, 23);
				}
				else
				{
					Write("Brak możliwych kart do doboru.", 1, 23);
				}
			}
			else
			{
				Write("Podano złe wertoci.", 1, 23);
			}
		}
		SprawdzStosy();
		_getch();
	} while (true);
}

void Gra::PokazStosy(int x, int y)
{
	{
		system("cls");
		for (int i = 0; i < 10; i++)
		{
			Write(i + 1, 8 * i + x + 1, y); //char(65+i)
			tab[i].PokazStosUkryty(8 * i + x, y + 2);
		}
	}
}

void Gra::SprawdzStosy()
{
	for (int i = 0; i < tab.size(); i++)
	{
		if (tab[i].SprawdzKolejnosc())
		{
			for (int j = 0; j < 13; j++)
			{
				tab[i].stos.pop_back();
			}
			punkty += 100;
		}
	}
}