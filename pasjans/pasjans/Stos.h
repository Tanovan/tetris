#pragma once
#include "Talia.h"
#include "Ustawienia.h"
class Stos : public Talia
{
public:
	vector <Karta> stos;
	
	/*
	Ma mie�:

		i mniej wi�cej takie funkcje

		sprawdza czy mo�na przenie�� karty z jednego stosu na inny. Je�eli mo�na, to przenie� i zwr�� true, je�eli nie to wy�witl komunikat czemu nie mo�na i zwr�� false.
		bool SprawdzCzyMoznaPrzeniesc(Stack *dokad, int ile); done i think
		Albo dopisz to w PrzeniesDo. Jak wygodniej.

	*/
	Stos();
	~Stos();
	void Dobierz(Talia *loc, int ile);
	void PokazStos(int x, int y);
	void PokazStosUkryty(int x, int y);
	bool PrzeniesDo(Stos *dokad, int ile);
	bool SprawdzCzyMoznaPrzeniesc(Stos *dokad, int ile);
	//To co jest ni�ej trzeba przerobi� �eby dzia�a�o dla tablicy stos�w i da� do klasy Gra. (Seba :D)
	//		 ||
	//		 ||
	//		\  /
	//		 \/
	//sprawdza kolejnosc czy karty le�� w dobrej kolejno�ci ( od do�u licz�c A 2 3 ... J Q K)
	bool SprawdzKolejnosc();
private:
	//Sprawdza czy karty s� dobrze kolorami u�o�one
	bool Check();
	//Spos�b sprawdzania kolejnosci kart
	//http://stackoverflow.com/questions/1380463/sorting-a-vector-of-custom-objects
	struct checkNumb
	{
		inline bool operator() (const Karta &karta1, const Karta &karta2)
		{
			return (karta1.numer > karta2.numer);
		}
	};
};

